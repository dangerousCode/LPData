# 车牌识别训练集

第一部分：自制车牌数据集

毕业论文所用的自制车牌数据集；
共计600张，涵盖30个省级行政区（安徽、香港、澳门以及台湾除外）。

有需要者，自行下载
链接：https://pan.baidu.com/s/17QQIVP_rUfpAl4xWuIIa-Q 
提取码：fsi7


第二部分：模拟车牌数据集

构建模拟车牌数据集方式，参考https://github.com/szad670401/end-to-end-for-chinese-plate-recognition


第三部分：其他车牌数据集介绍及下载

CCPD，参考https://blog.csdn.net/yang_daxia/article/details/88234138